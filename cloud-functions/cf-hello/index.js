const sayHello = (req, res) => res.send(`hello ${req.query.name} !`);

module.exports = {
    sayHello,
};
