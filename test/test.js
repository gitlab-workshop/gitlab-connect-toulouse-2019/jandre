const assert = require('assert');
var supertest = require("supertest");

const server = supertest.agent("https://us-central1-testgeotrend.cloudfunctions.net/sayHello");

describe('Array', function () {
    describe('#indexOf()', function () {
        it('should return -1 when the value is not present', function () {
            assert.equal([1, 2, 3].indexOf(4), -1);
        });
    });
});

describe('CF hello', () => {
    describe('#Say my name', () => {
        const name = 'Jerome';

        it(`should equal to "hello ${name} !"`, () => {
            server
                .get(`?name=${name}`)
                .end((err, res) => {
                    assert.equal(res.text, `hello ${name} !`);
                })
        })
    })
});